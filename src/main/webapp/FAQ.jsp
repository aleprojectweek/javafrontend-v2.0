<%@  page import="java.io.*,java.util.*,com.capgemini.model.User"
	language="java"
	contentType="text/html;                      
charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<title>FAQ Page</title>
<style>
body {
	background-color: white;
	text-align: center;
}
list-style-type: none;    
margin: 0;
padding: 0;
}
ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	width: 30px;
	background-color: #333;
	position: fixed;
}

li {
	float: center;
	display: inline-block;
}

li a {
	display: inline-block;
	color: black;
	text-align: centre;
	padding: 2px 10px;
	text-decoration: none;
	line-height: 100px;
	height: 80px;
	line-width: 150px;
	width: 190px;
}
table {
	width: 80%;
}

table, th, td {
	border: 2px solid black;
	margin-top: 50px;
	margin-left: 140px;
}

th {
	height: 50px;
	text-align: center;
}

td {
	height: 50px;
	vertical-align: bottom;
}

th, td {
	padding: 30px;
	text-align: center;
}
</style>
</head>
<body>
<img src="http://i.imgur.com/KQnd4Yl.jpg" alt="Logo" width="750"
		height="300">
		
		<%
 	if (session.getAttribute("currentLoggedInUser") != null) {
 %>
	<ul>
		<li><a href="Items.jsp">Items</a>
		<li>
		<li><a href="ShoppingCart.jsp">Basket</a>
		<li>
			<form action="SearchServlet">
				<input id="search-bar" name="search" type="text"
					placeholder="Search..."> <input id="search-button"
					name="search_submit" type="submit" value="Go">
			</form>
	</ul>
	
	<%
 		}
	%>
<h1> Frequently Asked Questions</h1><br>


  <h3>How do i find out about delivery prices?</h3>
 	<p>We have a delivery price page in which you can find out this information</p><br>
 	
  <h3>How long will delivery take?</h3>
 	<p>Normally within 3-5 working days</p><br>
 	
  <h3>User Guide for website</h3>
  <p><a href="https://www.google.co.uk/?gfe_rd=cr&ei=9kUbWIjnN6TS8AfP1KHYBg#q=user+guide">user guide</a></p>
 	
  

	
	<ul>
		<li><a href="FAQ.jsp">FAQ</a>
		<li>
		<li><a href="ContactUs.jsp">Contact Us</a>
		<li>
		<li><a href="Delivery.jsp">Delivery</a>
		<li>
		<%
 	if (session.getAttribute("currentLoggedInUser") == null) {
 %>
			<form>
				<input TYPE="button" VALUE="Login"
			onclick="window.location.href='Login.jsp'">
			</form>
			<%
 	}
			%>
			
			<%
 	if (session.getAttribute("currentLoggedInUser") != null) {
 %>
 			<form action="LogoutServlet">
				<input type="submit" value="Logout" />
			</form>
 <%
 	}
 %>
 
	</ul>
</body>
</html>