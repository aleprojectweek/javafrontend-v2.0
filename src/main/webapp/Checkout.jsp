<%@  page import="java.io.*,java.util.*,com.capgemini.model.User" language="java"
	contentType="text/html;                      
charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<title>Checkout Page</title>
<style>
body {
	background-color: white;
	text-align: center;
}
list-style-type: none;   
margin:0;
padding:0;
}
ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	width: 30px;
	background-color: #333;
	position: fixed;
}

li {
	float: center;
	display: inline-block;
}

li a {
	display: inline-block;
	color: black;
	text-align: centre;
	padding: 2px 10px;
	text-decoration: none;
	line-height: 100px;
	height: 80px;
	line-width: 150px;
	width: 190px;
}


table {
	width: 80%;
}

table, th, td {
	border: 2px solid black;
	margin-top: 100px;
	margin-left: 140px;
}

th {
	height: 50px;
	text-align: center;
}

td {
	height: 50px;
	vertical-align: bottom;
}

th, td {
	padding: 30px;
	text-align: center;
}
</style>
</head>
<body>

	<img src="http://i.imgur.com/KQnd4Yl.jpg" width="750" height="300" />

	<ul>
		<li><a href="Items.jsp">Items</a>
		<li>
		<li><a href="ShoppingCart.jsp">Basket</a>
		<li>
			<form action="SearchServlet">
				<input id="search-bar" name="search" type="text"
					placeholder="Search..." /> <input id="search-button"
					name="search_submit" type="submit" value="Go" />
			</form>
	</ul>

	<h1>&nbsp;Checkout</h1>

	<table>
	<%
		User user = (User) session.getAttribute("currentLoggedInUser");
		double price = (double) request.getAttribute("totalPrice");
		int shipping = (int) request.getAttribute("shippingCost");
	%>
		<tr>
			<th>UserId</th>
			<th>Sub Cost</th>
			<th>Shipping Cost</th>
			<th>Total Cost</th>
		</tr>

		<tr>
			<td><%=user.getUserID()%></td>
			<td><%=price%></td>
			<td><%=shipping%></td>
			<td><%=price + shipping%></td>

		</tr>
	</table><br>



	<br><form action="CheckoutServlet">
		Amount To Pay<br>
		<input type="text" name="billable" value=<%=price + shipping%> readonly><br>
		Card Type<br> <select name="Card Type" style="width: 405px">
			<option value="MasterCard">MasterCard</option>
			<option value="VisaDebit">VisaDebit</option>
			<option value="American Express">American Express</option>
		</select><br> 
			Card Number<br> 
			<input type="text" name="cardnumber" size="50"><br> 
			Card Expiration<br> 
			<select name='expireMM' id='expireMM' style="width: 200px">
			<option value=''>Month</option>
			<option value='01'>Janaury</option>
			<option value='02'>February</option>
			<option value='03'>March</option>
			<option value='04'>April</option>
			<option value='05'>May</option>
			<option value='06'>June</option>
			<option value='07'>July</option>
			<option value='08'>August</option>
			<option value='09'>September</option>
			<option value='10'>October</option>
			<option value='11'>November</option>
			<option value='12'>December</option>
		</select> <select name='expireYY' id='expireYY' style="width: 200px">
			<option value=''>Year</option>
			<option value='17'>2017</option>
			<option value='18'>2018</option>
			<option value='19'>2019</option>
			<option value='20'>2020</option>
			<option value='21'>2021</option>
			<option value='22'>2022</option>
		</select> <br> 
			Name on Card<br> 
			<input type="text" name="nameoncard" size="50"><br> 
			CVV<br> 
			<input type="text" name="securitycode" size="50"><br>
			Address Line 1<br>
			<input type="text" name="addressline1" size="50"><br>
			Address Line 2<br>
			<input type="text" name="addressline2" size="50"><br>
			County<br>
			<input type="text" name="county" size="50"><br>
			PostCode<br>
			<input type="text" name="postcode" size="50"><br>
		<br><input TYPE="submit" VALUE="Submit">
		<input TYPE="button" VALUE="Cancel"
			onclick="window.location.href='Homepage.jsp'">
	</form>

	<ul>
		<li><a href="FAQ.jsp">FAQ</a>
		<li>
		<li><a href="ContactUs.jsp">Contact Us</a>
		<li>
		<li><a href="Delivery.jsp">Delivery</a>
		<li>
			<form action="LogoutServlet">
				<input type="submit" value="Logout" />
			</form>
	</ul>
</body>
</html>
