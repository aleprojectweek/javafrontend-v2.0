<%@  page import="java.io.*,java.util.*" language="java"
	contentType="text/html;                      
charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "" "">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Totalitea Register Page</title>
<style>
body {
	background-color: white;
	text-align: center;
}

list-style-type
:
 
none
;

    
margin
:
 
0;
padding
:
 
0;
}
ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	width: 30px;
	background-color: #333;
	position: fixed;
}

li {
	float: center;
	display: inline-block;
}

li a {
	display: inline-block;
	color: black;
	text-align: centre;
	padding: 2px 10px;
	text-decoration: none;
	line-height: 100px;
	height: 80px;
	line-width: 150px;
	width: 190px;
}
</style>
</head>
<body>
	<img src="http://i.imgur.com/KQnd4Yl.jpg" alt="Logo" width="750"
		height="300">

	<h1>Registration</h1>

	<h2>Please enter details here</h2>
	<%
		if (request.getAttribute("feedback") != null) {
	%>
		<p><b><%=request.getAttribute("feedback")%></b></p>
	<%
		}
	%>
	<p>
		Passwords <b>must</b> contain a number and be between 8-16 characters
		long<br>Email addresses <b>must</b> contain a @ and be greater
		than 6 characters in length
	</p>

	<form action="RegistrationServlet">
		First Name:<br> <input type="text" name="firstname" size="50"><br>
		Last Name:<br> <input type="text" name="lastname" size="50"><br>
		Password:<br> <input type="password" name="password" size="50"><br>
		Email:<br> <input type="text" name="email" size="50"><br>
		Phone Number:<br> <input type="text" name="phonenumber" size="50"><br>
		<button>Submit</button>
		<input TYPE="button" VALUE="Cancel"
			onclick="window.location.href='Login.jsp'">
	</form>

	<ul>
		<li><a href="FAQ.jsp">FAQ</a>
		<li>
		<li><a href="ContactUs.jsp">Contact Us</a>
		<li>
		<li><a href="Delivery.jsp">Delivery</a>
		<li>
	</ul>
</body>
</html>
