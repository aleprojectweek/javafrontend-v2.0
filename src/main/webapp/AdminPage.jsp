<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<style>
body {background-color: white;
	  text-align: center;}
    list-style-type: none;
    margin: 0;
    padding: 0;
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    width: 30px;
    background-color: #333;
    position: fixed;
}

li {
    float: center;
    display: inline-block;
}

li a {
    display: inline-block;
    color: black;
    text-align: centre;
    padding: 2px 10px;
    text-decoration: none;
    line-height: 100px;
    height: 80px;
    line-width: 150px;
    width: 190px;
}


</style>

<title>Admin Page</title>
</head>
<body>
<img src="http://i.imgur.com/KQnd4Yl.jpg" width="750" height="300"/>

<ul>
<li><a href ="Items.jsp">Items</a><li>
<li><a href ="ShoppingCart.jsp">Basket</a><li>
<form action="SearchServlet">
<input id="search-bar" name="search" type="text" placeholder="Search..."/>
<input id="search-button" name="search_submit" type="submit" value="Go"/>
</form>
</ul>
 
<form action="RegisterProductServlet">
Product Name:<br>
<input type="text" name="productname" size="50"><br>
Supplier Name:<br>
<input type="text" name="suppliername" size="50"><br>
Product Type:<br>
<select name="Type">
<option value="Coffee">Coffee</option>
<option value="Caffeinated Tea">Caffeinated Tea</option>
<option value="Herbal Tea">Herbal Tea</option>
</select><br>
Caffeine Content(Coffee and Caffeinated Tea Only):<br>
<input type="text" name="caffeineContent"><br>
Brew Colour(Tea Only):<br>
<input type="text" name="brewColour"><br>
Number Of TeaBags(Tea Only):<br>
<input type="text" name="numOfTeaBags"><br>
Recommended Amount Per Mug (Coffee Only):<br>
<input type="text" name="recommendedDosage"><br>
Medicinal Use(Herbal Tea Only):<br>
<input type="text" name="medicinalUse"><br>
Price:<br>
<input type="text" name="productprice" size="50"><br>
Weight(g):<br>
<input type="text" name="productweight" size="50"><br>
<button>Submit</button>
</form>
 <ul>
<li><a href ="FAQ.jsp">FAQ</a><li>
<li><a href ="ContactUs.jsp">Contact Us</a><li>
<li><a href ="Delivery.jsp">Delivery</a><li>
</ul>
</body>
</html>