<%@  page import="java.io.*,java.util.*,com.capgemini.model.Product"
	language="java"
	contentType="text/html;                      
charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:include page="/ShoppingCartServlet" />
<!DOCTYPE html PUBLIC "" "">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Shopping Cart</title>
<style>
body {
	background-color: white;
	text-align: center;
}

list-style-type






:



 



none






;
margin






:






0;
padding






:



 



0;
}
ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	width: 30px;
	background-color: #333;
	position: fixed;
}

li {
	float: center;
	display: inline-block;
}

li a {
	display: inline-block;
	color: black;
	text-align: centre;
	padding: 2px 10px;
	text-decoration: none;
	line-height: 100px;
	height: 80px;
	line-width: 150px;
	width: 190px;
}

table {
	width: 80%;
}

table, th, td {
	margin-top: 100px;
	margin-left: 140px;
	border: 2px solid black;
}

th {
	height: 50px;
	text-align: center;
}

td {
	height: 50px;
	vertical-align: bottom;
}

th, td {
	padding: 30px;
	text-align: center;
}

.quantity {
	width: 25px;
}
}
</style>
</head>
<body>

	<img src="http://i.imgur.com/KQnd4Yl.jpg" alt="Logo" width="750"
		height="300">

	<ul>
		<li><a href="Items.jsp">Items</a>
		<li>
		<li><a href="ShoppingCart.jsp">Basket</a>
		<li>
			<form action="SearchServlet">
				<input id="search-bar" name="search" type="text"
					placeholder="Search..."> <input id="search-button"
					name="search_submit" type="submit" value="Go">
			</form>
	</ul>

	<h1>&nbsp; Currently in your shopping cart...</h1>
	<%
		if (request.getAttribute("feedback") != null) {
	%>
	<p><%=request.getAttribute("feedback")%></p>
	<%
		}
	%>
	<form action="CheckoutDelegationServlet">
		<%
			List<Product> products = (List<Product>) request.getAttribute("products");
			if (products != null) {
				Iterator<Product> ir = products.iterator();
				while (ir.hasNext()) {
					Product p = ir.next();
		%>
		<table>
			<tr>
				<th>Product Name</th>
				<th>Supplier Name</th>
				<th>Type</th>
				<th>Price</th>
				<th>Selected</th>
				<th>Quantity</th>
			</tr>
			<tr>
				<td><%=p.getProductName()%></td>
				<td><%=p.getSupplierName()%></td>
				<td><%=p.getType().getTypeName()%></td>
				<td><%=p.getPrice()%></td>
				<td><input type="checkbox" name="selected"
					value=<%=p.getProductID()%>></td>
				<td><input type="number"
					name=<%=p.getProductID() + "Quantity"%> min="1" max="99"
					class="quantity" value="1"></td>
			</tr>
		</table>
		<%
			}
			} else {
		%>
		<p>There are no items in your basket!</p>
		<%
			}
		%>
		<br> <br> <input TYPE="submit" name="action"
			VALUE="Remove Items"> <input TYPE="submit" name="action"
			VALUE="Checkout">
	</form>

	<ul>
		<li><a href="FAQ.jsp">FAQ</a>
		<li>
		<li><a href="ContactUs.jsp">Contact Us</a>
		<li>
		<li><a href="Delivery.jsp">Delivery</a>
		<li>
			<form action="LogoutServlet">
				<input type="submit" value="Logout" />
			</form>
	</ul>
</body>
</html>