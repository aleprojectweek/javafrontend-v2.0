<%@  page import="java.io.*,java.util.*,com.capgemini.model.User"
	language="java"
	contentType="text/html;                      
charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<title>Delivery</title>
<style>
body {
	background-color: white;
	text-align: center;
}
list-style-type: none;    
margin: 0;
padding: 0;
}
ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	width: 30px;
	background-color: #333;
	position: fixed;
}

li {
	float: center;
	display: inline-block;
}

li a {
	display: inline-block;
	color: black;
	text-align: centre;
	padding: 2px 10px;
	text-decoration: none;
	line-height: 100px;
	height: 80px;
	line-width: 150px;
	width: 190px;
}
table {
	width: 80%;
}

table, th, td {
	border: 2px solid black;
	margin-top: 50px;
	margin-left: 140px;
}

th {
	height: 50px;
	text-align: center;
}

td {
	height: 50px;
	vertical-align: bottom;
}

th, td {
	padding: 30px;
	text-align: center;
}
</style>
</head>
<body>
<img src="http://i.imgur.com/KQnd4Yl.jpg" alt="Logo" width="750"
		height="300">
	<%
 	if (session.getAttribute("currentLoggedInUser") != null) {
 %>
	<ul>
		<li><a href="Items.jsp">Items</a>
		<li>
		<li><a href="ShoppingCart.jsp">Basket</a>
		<li>
			<form action="SearchServlet">
				<input id="search-bar" name="search" type="text"
					placeholder="Search..."> <input id="search-button"
					name="search_submit" type="submit" value="Go">
			</form>
	</ul>
	
	<%
 		}
	%>
<h1> Delivery Information</h1>
<table>
	<tr>
		<th>Weight</th>
		<th>Price</th>
	</tr>
	<tr>
		<td>Less than 5kg</td>
		<td>�1</td>
		</tr>
		<tr>
		<td> Between 5kg and 10kg</td>
		<td>�10</td>
		</tr>
		<tr>
		<td>Over 10kg</td>
		<td>�15</td>
		</tr>
		</table>
	<h3> If price is more than �100 then delivery is free!</h3>
	
	<ul>
		<li><a href="FAQ.jsp">FAQ</a>
		<li>
		<li><a href="ContactUs.jsp">Contact Us</a>
		<li>
		<li><a href="Delivery.jsp">Delivery</a>
		<li>
			<%
 	if (session.getAttribute("currentLoggedInUser") == null) {
 %>
			<form>
				<input TYPE="button" VALUE="Login"
			onclick="window.location.href='Login.jsp'">
			</form>
			<%
 	}
			%>
			
			<%
 	if (session.getAttribute("currentLoggedInUser") != null) {
 %>
 			<form action="LogoutServlet">
				<input type="submit" value="Logout" />
			</form>
 <%
 	}
 %>
	</ul>
</body>
</html>
