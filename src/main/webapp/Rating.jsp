<%@  page import="java.io.*,java.util.*,com.capgemini.model.User"
	language="java"
	contentType="text/html;                      
charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 

<title>Rating Page</title>
<style>
body {
	background-color: white;
	text-align: center;
}
list-style-type: none;    
margin: 0;
padding: 0;
}
ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	width: 30px;
	background-color: #333;
	position: fixed;
}

li {
	float: center;
	display: inline-block;
}

li a {
	display: inline-block;
	color: black;
	text-align: centre;
	padding: 2px 10px;
	text-decoration: none;
	line-height: 100px;
	height: 80px;
	line-width: 150px;
	width: 190px;
}

.rating-wrapper {
  overflow: hidden;
  display: inline-block;
}

.rating-input {
  position: absolute;
  left: 0;
  top: -50px;
}

.rating-star:hover,
.rating-star:hover ~ .rating-star {
  background-position: 0 0;
}

.rating-wrapper:hover .rating-star:hover,
.rating-wrapper:hover .rating-star:hover ~ .rating-star,
.rating-input:checked ~ .rating-star {
  background-position: 0 0;
}

.rating-star,
.rating-wrapper:hover .rating-star {
  float: right;
  display: block;
  width: 16px;
  height: 16px;
  background: url('http://css-stars.com/wp-content/uploads/2013/12/stars.png') 0 -16px;
}

</style>
</head>
<body>

	<img src="http://i.imgur.com/KQnd4Yl.jpg" width="750" height="300" />
	<ul>
		<li><a href="Items.jsp">Items</a>
		<li>
		<li><a href="ShoppingCart.jsp">Basket</a>
		<li>
			<form action="SearchServlet">
				<input id="search-bar" name="search" type="text"
					placeholder="Search..." /> <input id="search-button"
					name="search_submit" type="submit" value="Go" />
			</form>
	</ul>
	
	<h1> Please rate your experience!</h1>
	
<div class="rating-wrapper">
  <input type="radio" class="rating-input" id="rating-input-1-5" name="rating-input-1" />
  <label for="rating-input-1-5" class="rating-star"></label>
  <input type="radio" class="rating-input" id="rating-input-1-4" name="rating-input-1" />
  <label for="rating-input-1-4" class="rating-star"></label>
  <input type="radio" class="rating-input" id="rating-input-1-3" name="rating-input-1" />
  <label for="rating-input-1-3" class="rating-star"></label>
  <input type="radio" class="rating-input" id="rating-input-1-2" name="rating-input-1" />
  <label for="rating-input-1-2" class="rating-star"></label>
  <input type="radio" class="rating-input" id="rating-input-1-1" name="rating-input-1" />
  <label for="rating-input-1-1" class="rating-star"></label>
</div><br>

<br><input TYPE="button" VALUE="Submit"
        onclick="window.location.href='ResponsePage.jsp'">

	<ul>
		<li><a href="FAQ.jsp">FAQ</a>
		<li>
		<li><a href="ContactUs.jsp">Contact Us</a>
		<li>
		<li><a href="Delivery.jsp">Delivery</a>
		<li>
		<form action="LogoutServlet">
		<input type="submit" value="Logout" />
		</form> 
		
	</ul>
</body>
</html>
