package com.capgemini.util;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.capgemini.model.Dessert;
import com.capgemini.model.Product;
import com.capgemini.model.ProductSale;
import com.capgemini.model.User;

public class HibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	public static SessionFactory buildSessionFactory() {
		try {
			return new Configuration()
					.configure()
					.addPackage("com.capgemini.model")
					.addAnnotatedClass(User.class)
					.addAnnotatedClass(Product.class)
					.addAnnotatedClass(ProductSale.class)
					.addAnnotatedClass(Dessert.class)
					.buildSessionFactory();
		} catch (Throwable ex) {
			System.out.println("Encountered an error when creating the session factory!");
			System.out.println(ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}
	
	public static Session getSession() {
		return sessionFactory.openSession();
	}
	
	public static EntityManager getEntityManager() {
		return sessionFactory.createEntityManager();
	}
}