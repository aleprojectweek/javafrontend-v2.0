package com.capgemini.util;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.exception.GenericJDBCException;

import com.capgemini.model.Product;
import com.capgemini.model.User;
import com.capgemini.model.types.CaffeinatedTea;
import com.capgemini.model.types.Coffee;
import com.capgemini.model.types.HerbalTea;

public class DBQueryManager {

	public static User checkLogin(String username, String password) {
		EntityManager eM = HibernateUtil.getEntityManager();
		List<User> users = eM
				.createQuery(
						"Select u " + "FROM User u " + "WHERE " + "u.email = :username AND " + "u.password = :password",
						User.class)
				.setParameter("username", username).setParameter("password", password).getResultList();
		if (users.isEmpty()) {
			return null;
		} else {
			return users.get(0);
		}
	}

	public static int insertUser(String firstName, String lastName, String email, String phoneNumber, String password)
			throws PLSQLException {
		int userID = -1;
		EntityManager eM = HibernateUtil.getEntityManager();
		eM.getTransaction().begin();
		try {
			StoredProcedureQuery query = eM.createStoredProcedureQuery("insertUser")
					.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(6, Integer.class, ParameterMode.OUT)
					.setParameter(1, password).setParameter(2, firstName)
					.setParameter(3, lastName).setParameter(4, email)
					.setParameter(5, phoneNumber);
			query.execute();
			userID = (int) query.getOutputParameterValue(6);
			eM.getTransaction().commit();
		} catch (Exception ex) {
			if (ex.getCause() != null && ex.getCause() instanceof GenericJDBCException) {
				GenericJDBCException gEx = (GenericJDBCException) ex.getCause();
				if (gEx.getCause() != null && gEx.getCause() instanceof SQLException) {
					SQLException sqlEx = (SQLException) gEx.getCause();

					if (sqlEx.getErrorCode() == 20001) {
						PLSQLException plsqlEx = new PLSQLException("Invalid password!", sqlEx.getMessage());
						throw plsqlEx;
					}
					if (sqlEx.getErrorCode() == 20002) {
						PLSQLException plsqlEx = new PLSQLException("Invalid phone number!", sqlEx.getMessage());
						throw plsqlEx;
					}
					if (sqlEx.getErrorCode() == 20003) {
						PLSQLException plsqlEx = new PLSQLException("Invalid email!", sqlEx.getMessage());
						throw plsqlEx;
					}
				}
			}
		}
		return userID;
	}
	
	public static int insertProduct(String productName, String supplierName, String typeName, double caffeineContent, String brewColour, int numOfTeaBags, double recommendedDosage, String medicinalUse , double productPrice, int weight) throws PLSQLException {
		int productID = -1;
		String type = "";
		if(typeName.equalsIgnoreCase("Coffee")) {
			Coffee c = new Coffee(typeName, recommendedDosage, caffeineContent);
			type = c.toString();
		}
		if(typeName.equalsIgnoreCase("Herbal Tea")) {
			HerbalTea t = new HerbalTea(typeName, numOfTeaBags, brewColour, medicinalUse);
			type = t.toString();
		}
		if(typeName.equalsIgnoreCase("Caffeinated Tea")) {
			CaffeinatedTea t = new CaffeinatedTea(typeName, numOfTeaBags, brewColour, caffeineContent);
			type = t.toString();
		}
		EntityManager eM = HibernateUtil.getEntityManager();
		eM.getTransaction().begin();
		try {
		StoredProcedureQuery query = eM.createStoredProcedureQuery("insertProduct")
				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, Double.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter(6, Integer.class, ParameterMode.OUT)
				.setParameter(1, productName)
				.setParameter(2, supplierName)
				.setParameter(3, type)
				.setParameter(4, productPrice)
				.setParameter(5, weight);
		query.execute();
		productID = (int) query.getOutputParameterValue(6);
		eM.getTransaction().commit();
		} catch (Exception ex) {
			if (ex.getCause() != null && ex.getCause() instanceof GenericJDBCException) {
				GenericJDBCException gEx = (GenericJDBCException) ex.getCause();
				if (gEx.getCause() != null && gEx.getCause() instanceof SQLException) {
					SQLException sqlEx = (SQLException) gEx.getCause();

					if (sqlEx.getErrorCode() == 20001) {
						PLSQLException plsqlEx = new PLSQLException("Invalid password!", sqlEx.getMessage());
						throw plsqlEx;
					}
					if (sqlEx.getErrorCode() == 20002) {
						PLSQLException plsqlEx = new PLSQLException("Invalid phone number!", sqlEx.getMessage());
						throw plsqlEx;
					}
					if (sqlEx.getErrorCode() == 20003) {
						PLSQLException plsqlEx = new PLSQLException("Invalid email!", sqlEx.getMessage());
						throw plsqlEx;
					}
				}
			}
		}
		return productID;
	}

	public static void insertProductSale(Integer userId, Integer productId, Integer quantity) throws SQLException
	{
		EntityManager em = HibernateUtil.getEntityManager();
		em.getTransaction().begin();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String dateString = dateFormat.format(cal.getTime());
		Timestamp date = null;
		try {
			date = new Timestamp((dateFormat.parse(dateString)).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(date.toString());
		StoredProcedureQuery query = em.createStoredProcedureQuery("insertProductSale")
				.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, java.sql.Date.class, ParameterMode.IN)
				.setParameter(1, userId)
				.setParameter(2, productId)
				.setParameter(3, quantity)
				.setParameter(4, date);
		query.execute();
		em.getTransaction().commit();
	}
	
	public static List<Product> getAllProducts() {
		EntityManager eM = HibernateUtil.getEntityManager();
		List<Product> products = eM.createQuery("SELECT p " + "FROM Product p", Product.class).getResultList();
		for (Product p : products) {
			p.convertType();
		}
		return products;
	}

	public static List<Product> getMatchingProducts(String filter) {
		List<Product> products = getAllProducts();
		if (filter.equalsIgnoreCase("coffee")) {
			// products.removeIf(p ->
			// !p.getType().getTypeName().equalsIgnoreCase(filter));
			List<Product> filteredProducts = products.stream()
					.filter(p -> p.getType().getTypeName().equalsIgnoreCase(filter)).collect(Collectors.toList());
			return filteredProducts;
		}
		if (filter.equalsIgnoreCase("caffeinated tea")) {
			List<Product> filteredProducts = products.stream()
					.filter(p -> p.getType().getTypeName().equalsIgnoreCase(filter)).collect(Collectors.toList());
			return filteredProducts;
		}
		if (filter.equalsIgnoreCase("herbal tea")) {
			List<Product> filteredProducts = products.stream()
					.filter(p -> p.getType().getTypeName().equalsIgnoreCase(filter)).collect(Collectors.toList());
			return filteredProducts;
		}
		List<Product> filteredProducts = products.stream().filter(p -> {
			if (productNameContains(p, filter) || supplierNameContains(p, filter)) {
				return true;
			}
			return false;
		}).collect(Collectors.toList());
		// List<Product> filteredProducts = products.stream().filter(p ->
		// p.getProductName().contains(filter) || p ->
		// p.getSupplierName().contains(filter)).collect(Collectors.toList());
		return filteredProducts;
	}

	public static List<Product> getProductByID(int[] ids) {
		List<Product> products = getAllProducts();
		List<Product> filteredList = products.stream().filter(p -> compareID(p, ids)).collect(Collectors.toList());
		return filteredList;
	}

	private static boolean compareID(Product p, int[] ids) {
		Integer id = p.getProductID();
		for (int i : ids) {
			if (i == id) {
				return true;
			}
		}
		return false;
	}

	private static boolean supplierNameContains(Product p, String filter) {
		if (caseinsensitiveContains(p.getSupplierName(), filter)) {
			return true;
		}
		return false;
	}

	private static boolean productNameContains(Product p, String filter) {
		if (caseinsensitiveContains(p.getProductName(), filter)) {
			return true;
		}
		return false;
	}

	private static boolean caseinsensitiveContains(String s1, String s2) {
		return Pattern.compile(Pattern.quote(s2), Pattern.CASE_INSENSITIVE).matcher(s1).find();
	}
}