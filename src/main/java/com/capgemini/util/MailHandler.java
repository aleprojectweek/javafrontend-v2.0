package com.capgemini.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailHandler {

	Session mailSession;
	
	private void setMailServerProperties() {
		Properties emailProperties = new Properties();
		emailProperties.put("mail.smtp.host", "smtp.gmail.com");
		emailProperties.put("mail.smtp.user", "totali.tea123");
		emailProperties.put("mail.smtp.password", "Example123");
		emailProperties.put("mail.smtp.port", "587");
        emailProperties.put("mail.smtp.auth", "true");
        emailProperties.put("mail.smtp.starttls.enable", "true");
        
        mailSession = Session.getDefaultInstance(emailProperties, null);
	}
	
	public MailHandler() {
		setMailServerProperties();
	}
	
	private MimeMessage draftMessage(String subject, String body, String recipient)
			throws MessagingException {
        MimeMessage emailMessage = new MimeMessage(mailSession);
        emailMessage.addFrom(InternetAddress.parse("totali.tea123@gmail.com"));
        emailMessage.addRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
        emailMessage.setSubject(subject);
        emailMessage.setText(body);
        return emailMessage;
	}
	
	public void sendEmail(String subject, String body, String recipient)
			throws MessagingException {
		Transport transport = mailSession.getTransport("smtp");
		MimeMessage message = draftMessage(subject, body, recipient);
		transport.connect("smtp.gmail.com", "totali.tea123", "Example123");
		transport.sendMessage(message, message.getAllRecipients());
		System.out.println("Email sent successfully!");
	}

}
