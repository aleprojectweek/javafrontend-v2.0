package com.capgemini.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class OrderOutput {

	private BufferedWriter writer;

	public OrderOutput(String fileName) {
		File file = new File("C:\\Orders\\", fileName);
		if(file.exists()) {
			try {
				writer = new BufferedWriter(new FileWriter(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				file.createNewFile();
				writer = new BufferedWriter(new FileWriter(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void write(String string) {
		if (writer != null) {
			try {
				writer.write(string);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public BufferedWriter getWriter() {
		return writer;
	}

	public void closeWriter() {
		if (writer != null) {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
