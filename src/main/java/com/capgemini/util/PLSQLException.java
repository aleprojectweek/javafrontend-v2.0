package com.capgemini.util;

import java.sql.SQLException;

public class PLSQLException extends SQLException {

	private static final long serialVersionUID = 8387244653395940417L;
	
	private String sqlErrorMessage;
	
	public PLSQLException(String sqlErrorMessage, String message) {
		super(message);
		this.sqlErrorMessage = sqlErrorMessage;
	}

	/**
	 * @return the errorMessage
	 */
	public String getSqlErrorMessage() {
		return sqlErrorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setSqlErrorMessage(String errorMessage) {
		this.sqlErrorMessage = errorMessage;
	}
	
	
}
