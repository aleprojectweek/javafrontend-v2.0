package com.capgemini.serverlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckoutDelegationServlet extends HttpServlet {

	private static final long serialVersionUID = 8857120986784379134L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		String action = request.getParameter("action");
		
		if("Remove Items".equals(action)) {
			try {
				request.getRequestDispatcher("/RemoveItemsFromBasketServlet").forward(request, response);
			} catch (ServletException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if("Checkout".equals(action)) {
			try {
				request.getRequestDispatcher("/TotalCostServlet").forward(request, response);
			} catch (ServletException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
