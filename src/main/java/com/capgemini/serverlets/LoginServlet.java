package com.capgemini.serverlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.capgemini.model.User;
import com.capgemini.util.DBQueryManager;

public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -526829374350861160L;

	/*
	 * Implementation for loginservlet
	 */
	public void doGet(HttpServletRequest  request, HttpServletResponse response) throws ServletException {
		User user = DBQueryManager.checkLogin(request.getParameter("email"), request.getParameter("password"));
		if(user != null) {
			try {
				response.sendRedirect("Homepage.jsp");
				HttpSession session = request.getSession(true);
				session.setAttribute("currentLoggedInUser", user);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				response.sendRedirect("IncorrectLogin.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
