package com.capgemini.serverlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.util.DBQueryManager;
import com.capgemini.util.PLSQLException;

public class RegistrationServlet extends HttpServlet {

	private static final long serialVersionUID = -2696589429182580843L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int userID = -1;
		String errorMessage = "";
		try {
			userID = DBQueryManager.insertUser(request.getParameter("firstname"), request.getParameter("lastname"),
					request.getParameter("email"), request.getParameter("phonenumber"),
					request.getParameter("password"));
		} catch (PLSQLException e) {
			errorMessage = e.getSqlErrorMessage();
		}
		if (userID != -1) {
			response.sendRedirect("Login.jsp");
		} else {
			request.setAttribute("feedback",
					errorMessage);
			try {
				request.getRequestDispatcher("/Register.jsp").forward(request, response);
			} catch (ServletException e) {
				e.printStackTrace();
			}
		}
	}

}
