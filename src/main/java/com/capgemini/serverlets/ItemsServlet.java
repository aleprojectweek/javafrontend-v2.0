package com.capgemini.serverlets;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.util.DBQueryManager;

public class ItemsServlet extends HttpServlet {

	private static final long serialVersionUID = -2263236017441913354L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("products", DBQueryManager.getAllProducts());
		try {
			response.sendRedirect("Items.jsp");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
