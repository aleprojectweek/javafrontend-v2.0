package com.capgemini.serverlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.util.DBQueryManager;
import com.capgemini.util.PLSQLException;

public class RegisterProductServlet extends HttpServlet {

	private static final long serialVersionUID = 8633026520064540078L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		int productID = -1;
		String errorMessage = "";
		try {
			productID = DBQueryManager.insertProduct(request.getParameter("productname"),
					request.getParameter("suppliername"), request.getParameter("Type"),
					Double.valueOf(request.getParameter("caffeineContent")).doubleValue(),
					request.getParameter("brewColour"),
					Integer.valueOf(request.getParameter("numOfTeaBags")).intValue(),
					Double.valueOf(request.getParameter("recommendedDosage")).doubleValue(),
					request.getParameter("medicinalUse"),
					Double.valueOf(request.getParameter("productprice")).doubleValue(),
					Integer.valueOf(request.getParameter("productweight")).intValue());
		} catch (PLSQLException e) {
			errorMessage = e.getSqlErrorMessage();
		}
		if (productID != -1) {
			try {
				response.sendRedirect("AdminPage.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			request.setAttribute("feedback", errorMessage);
			try {
				request.getRequestDispatcher("/AdminPage.jsp").forward(request, response);
			} catch (IOException | ServletException e) {
				e.printStackTrace();
			}

		}
	}
}