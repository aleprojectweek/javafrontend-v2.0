package com.capgemini.serverlets;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.capgemini.model.Product;
import com.capgemini.model.User;

public class TotalCostServlet extends HttpServlet {
	
	private static final long serialVersionUID = 9195299998847696035L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("currentLoggedInUser");
		ArrayList<Product> basket = user.getBasket();
		double totalPrice = 0.0;
		int totalWeight = 0;
		ArrayList<String> quantities = new ArrayList<String>();
		for(Product p: basket) {
			String quantityString = request.getParameter(p.getProductID() + "Quantity");
			int quantity = Integer.valueOf(quantityString);
			quantities.add(p.getProductID() + ":" + quantity);
			totalPrice += p.getPrice() * quantity;
			totalWeight += p.getWeightInGrams();
		}
		totalPrice = round(totalPrice, 2);
		session.setAttribute("quants", quantities);
		request.setAttribute("totalPrice", totalPrice);
		if(!(totalPrice >= 100)) {
			if(totalWeight <= 5000) {
				request.setAttribute("shippingCost", 1);
			} else if(totalWeight <= 10000 && totalWeight > 5000) {
				request.setAttribute("shippingCost", 10);
			} else if(totalWeight > 10000) {
				request.setAttribute("shippingCost", 15);
			}
		} else {
			request.setAttribute("shippingCost", 0);
		}
		try {
			request.getRequestDispatcher("/Checkout.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			e.printStackTrace();
		}
	}

	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}