package com.capgemini.serverlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.capgemini.model.Product;
import com.capgemini.model.User;
import com.capgemini.util.DBQueryManager;

public class RemoveItemsFromBasketServlet extends HttpServlet {

	private static final long serialVersionUID = -2604677439466663086L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		String[] selectedIds = request.getParameterValues("selected");
		if (selectedIds != null) {
			int[] ids = new int[selectedIds.length];
			for (int x = 0; x < selectedIds.length; x++) {
				Integer id = Integer.valueOf(selectedIds[x]);
				ids[x] = id;
			}
			List<Product> selectedProducts = DBQueryManager.getProductByID(ids);
			HttpSession session = request.getSession(true);
			User user = (User) session.getAttribute("currentLoggedInUser");
			ArrayList<Product> basket = user.getBasket();
			if (user.getBasket() != null) {
				for(Product selectedProduct: selectedProducts) {
					basket.removeIf(p -> p.getProductID() == selectedProduct.getProductID());
				}
				user.setBasket(basket);
			}
			session.setAttribute("currentLoggedInUser", user);
			try {
				response.sendRedirect("ShoppingCart.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
