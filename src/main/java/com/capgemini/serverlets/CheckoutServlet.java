package com.capgemini.serverlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.model.Product;
import com.capgemini.model.User;
import com.capgemini.util.DBQueryManager;
import com.capgemini.util.MailHandler;
import com.capgemini.util.OrderOutput;

public class CheckoutServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		User user = (User) request.getSession(true).getAttribute("currentLoggedInUser");
		ArrayList<String> quantities = (ArrayList<String>) request.getSession(true).getAttribute("quants");
		HashMap<Integer, Integer> productQuants = new HashMap<Integer,Integer>();
		for(String s: quantities) {
			String[] parts = s.split(":");
			productQuants.put(Integer.valueOf(parts[0]), Integer.valueOf(parts[1]));
		}
		for (Product p : user.getBasket()) {
			try {
				DBQueryManager.insertProductSale(user.getUserID(), p.getProductID(), productQuants.get(p.getProductID()).intValue());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		ArrayList<Product> basket = user.getBasket();
		String toWrite = "Order details:\n";
		for(Product p:basket) {
			toWrite += p.getProductID() + ": " + p.getProductName() + ", Quantity: " + productQuants.get(p.getProductID()) + "\n";
		}
		toWrite += "Total Paid: " + request.getParameter("billable") + "\n";
		toWrite += "Shipping Address:\n" + request.getParameter("addressline1") + "\n" + request.getParameter("addressline2") + "\n" + request.getParameter("county") + "\n" + request.getParameter("postcode");
		OrderOutput output = new OrderOutput("Order" + System.currentTimeMillis() + ".txt");
		output.write(toWrite);
		output.closeWriter();
		MailHandler mail = new MailHandler();
		try {
			mail.sendEmail("Receipt", toWrite, user.getEmail());
		} catch (MessagingException e1) {
			e1.printStackTrace();
		}
		basket.clear();
		user.setBasket(basket);
		request.getSession(true).setAttribute("currentLoggedInUser", user);
		try {
			request.getRequestDispatcher("/Rating.jsp").forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}

}
