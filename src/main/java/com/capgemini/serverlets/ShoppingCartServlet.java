package com.capgemini.serverlets;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.capgemini.model.User;

public class ShoppingCartServlet extends HttpServlet{

	private static final long serialVersionUID = -8883998200239641902L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("currentLoggedInUser");
		request.setAttribute("products", user.getBasket());
		try {
			response.sendRedirect("ShoppingCart.jsp");
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

}
