package com.capgemini.serverlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.util.DBQueryManager;

public class SearchServlet extends HttpServlet {

	private static final long serialVersionUID = -3451578027968495294L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		String input = request.getParameter("search");
		if(!(input == null)) {
			request.setAttribute("products", DBQueryManager.getMatchingProducts(input));
			try {
				getServletContext().getRequestDispatcher("/SearchPage.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				e.printStackTrace();
			}
		} else {
			request.setAttribute("products", DBQueryManager.getAllProducts());
			try {
				getServletContext().getRequestDispatcher("/SearchPage.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				e.printStackTrace();
			}
		}
	}

}