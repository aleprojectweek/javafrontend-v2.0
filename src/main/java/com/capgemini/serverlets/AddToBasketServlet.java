package com.capgemini.serverlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.capgemini.model.Product;
import com.capgemini.model.User;
import com.capgemini.util.DBQueryManager;

public class AddToBasketServlet extends HttpServlet {

	private static final long serialVersionUID = 8504604887471952706L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		String[] selectedIds = request.getParameterValues("selected");
		if (selectedIds != null) {
			int[] ids = new int[selectedIds.length];
			for (int x = 0; x < selectedIds.length; x++) {
				Integer id = Integer.valueOf(selectedIds[x]);
				ids[x] = id;
			}
			List<Product> selectedProducts = DBQueryManager.getProductByID(ids);
			HttpSession session = request.getSession(true);
			User user = (User) session.getAttribute("currentLoggedInUser");
			if (user.getBasket() == null) {
				ArrayList<Product> basket = new ArrayList<Product>();
				basket.addAll(selectedProducts);
				user.setBasket(basket);
				request.setAttribute("feedback", "Items added to the shopping cart successfully!");
				session.setAttribute("currentLoggedInUser", user);
				try {
					request.getRequestDispatcher("/ShoppingCart.jsp").forward(request, response);
				} catch (IOException | ServletException e) {
					e.printStackTrace();
				}
			} else {
				if (compareIDS(user.getBasket(), selectedProducts)) {
					user.getBasket().addAll(selectedProducts);
					request.setAttribute("feedback", "Items added to the shopping cart successfully!");
					session.setAttribute("currentLoggedInUser", user);
					try {
						request.getRequestDispatcher("/ShoppingCart.jsp").forward(request, response);
					} catch (IOException | ServletException e) {
						e.printStackTrace();
					}
				} else {
					request.setAttribute("feedback", "Item(s) already in the cart!");
					session.setAttribute("currentLoggedInUser", user);
					try {
						request.getRequestDispatcher("/ShoppingCart.jsp").forward(request, response);
					} catch (ServletException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			try {
				response.sendRedirect("Items.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean compareIDS(ArrayList<Product> basket, List<Product> selectedProducts) {
		ArrayList<Integer> selectedIDs = getProductIDS(selectedProducts);
		for (Product p : basket) {
			int pID = p.getProductID();
			if (selectedIDs.contains(pID)) {
				return false;
			}
		}
		return true;
	}

	private ArrayList<Integer> getProductIDS(List<Product> productList) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Product p : productList) {
			int pID = p.getProductID();
			ids.add(pID);
		}
		return ids;
	}
}