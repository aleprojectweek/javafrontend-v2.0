package com.capgemini.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "UserInfo")
public class User {

	// Declare class fields
	private int userID;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String password;
	@Transient
	private ArrayList<Product> basket;

	public User() {
		// Do nothing
	}

	/**
	 * Constructor for the user object
	 * 
	 * @param userID
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phoneNumber
	 */
	public User(int userID, String firstName, String lastName, String email, String phoneNumber, String password) {
		this(firstName, lastName, email, phoneNumber, password);
		this.userID = userID;
		basket = new ArrayList<Product>();
	}

	/**
	 * Constructor for the user object without the userId
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phoneNumber
	 */
	public User(String firstName, String lastName, String email, String phoneNumber, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.password = password;
		basket = new ArrayList<Product>();
	}

	public boolean checkLogin(String username, String password) {
		// TODO Implement the ability to check a users login details.
		return false;
	}

	/**
	 * @return the userID
	 */
	@Id
	@GeneratedValue
	@Column(name = "UserID")
	public int getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	/**
	 * @return the firstName
	 */
	@Column(name = "FName")
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	@Column(name = "LName")
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	@Column(name = "Email")
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phoneNumber
	 */
	@Column(name = "PhoneNumber")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * 
	 * @return the password
	 */
	@Column(name = "Password")
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the basket
	 */
	@Transient
	public ArrayList<Product> getBasket() {
		return basket;
	}

	/**
	 * @param basket the basket to set
	 */
	public void setBasket(ArrayList<Product> basket) {
		this.basket = basket;
	}
}