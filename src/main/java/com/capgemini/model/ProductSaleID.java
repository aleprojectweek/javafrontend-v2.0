package com.capgemini.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ProductSaleID implements Serializable {

	private static final long serialVersionUID = -1688313950442628459L;

	private int userID;
	private int productID;

	/**
	 * Constructor for the ProductSaleID object
	 * 
	 * @param userID
	 * @param productID
	 */
	public ProductSaleID(int userID, int productID) {
		this.userID = userID;
		this.productID = productID;
	}

	/**
	 * @return the userID
	 */
	@Column(name = "UserID")
	public int getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	/**
	 * @return the productID
	 */
	@Column(name = "ProductID")
	public int getProductID() {
		return productID;
	}

	/**
	 * @param productID
	 *            the productID to set
	 */
	public void setProductID(int productID) {
		this.productID = productID;
	}
}