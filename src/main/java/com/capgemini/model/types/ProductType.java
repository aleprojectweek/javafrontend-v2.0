package com.capgemini.model.types;

import java.io.Serializable;

public abstract class ProductType implements Serializable{
	
	private static final long serialVersionUID = -6786831386900000460L;

	protected String typeName;
	
	public ProductType(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	public String toString() {
		String toReturn = typeName;
		return toReturn;
	}
	
	public static ProductType fromString(String string) {
		//TODO ADD CHECK FOR NUMBER OF PARTS
		string = string.toLowerCase().trim();
		String[] parts = string.split("::");
		ProductType object = null;
		if(parts.length == 1) {
			System.out.println("You cannot instantaite this type!");
		}
		if(parts.length ==3 && parts[0].equalsIgnoreCase("tea")) {
			object = new Tea(parts[0], Integer.valueOf(parts[1]), parts[2]);
		}
		if(parts.length ==3 && parts[0].equalsIgnoreCase("coffee")) {
			object = new Coffee(parts[0], Double.valueOf(parts[1]), Double.valueOf(parts[2]));
		}
		if(parts.length ==4 && parts[0].equalsIgnoreCase("herbal tea")) {
			object = new HerbalTea(parts[0], Integer.valueOf(parts[1]), parts[2], parts[3]);
		}
		if(parts.length ==4 && parts[0].equalsIgnoreCase("caffeinated tea")) {
			object = new CaffeinatedTea(parts[0], Integer.valueOf(parts[1]), parts[2], Double.valueOf(parts[3]));
		}
		return object;
	}
}