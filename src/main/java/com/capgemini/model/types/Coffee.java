package com.capgemini.model.types;

public class Coffee extends ProductType {

	private static final long serialVersionUID = 899711112727413390L;

	protected double recommendedAmountPerMug;
	protected double caffeineContent;

	public Coffee(String typeName, double recommendedAmountPerMug, double caffeineContent) {
		super(typeName);
		this.recommendedAmountPerMug = recommendedAmountPerMug;
		this.caffeineContent = caffeineContent;
	}

	/**
	 * @return the recommendedAmountPerMug
	 */
	public double getRecommendedAmountPerMug() {
		return recommendedAmountPerMug;
	}

	/**
	 * @param recommendedAmountPerMug
	 *            the recommendedAmountPerMug to set
	 */
	public void setRecommendedAmountPerMug(double recommendedAmountPerMug) {
		this.recommendedAmountPerMug = recommendedAmountPerMug;
	}

	/**
	 * @return the caffeineContent
	 */
	public double getCaffeineContent() {
		return caffeineContent;
	}

	/**
	 * @param caffeineContent the caffeineContent to set
	 */
	public void setCaffeineContent(double caffeineContent) {
		this.caffeineContent = caffeineContent;
	}

	@Override
	public String toString() {
		String toReturn = super.toString();
		toReturn += "::" + recommendedAmountPerMug + "::" + caffeineContent;
		return toReturn;
	}
}