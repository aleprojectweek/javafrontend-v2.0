package com.capgemini.model.types;

public class Tea extends ProductType {
	
	private static final long serialVersionUID = -7925799614486961664L;
	
	protected int numOfTeaBags;
	protected String brewColour;
	
	public Tea(String typeName, int numOfTeaBags, String brewColour) {
		super(typeName);
		this.numOfTeaBags = numOfTeaBags;
		this.brewColour = brewColour;
	}

	/**
	 * @return the numOfTeaBags
	 */
	public int getNumOfTeaBags() {
		return numOfTeaBags;
	}

	/**
	 * @param numOfTeaBags the numOfTeaBags to set
	 */
	public void setNumOfTeaBags(int numOfTeaBags) {
		this.numOfTeaBags = numOfTeaBags;
	}
	
	/**
	 * @return the brewColour
	 */
	public String getBrewColour() {
		return brewColour;
	}

	/**
	 * @param brewColour the brewColour to set
	 */
	public void setBrewColour(String brewColour) {
		this.brewColour = brewColour;
	}

	@Override
	public String toString() {
		String toReturn = super.toString();
		toReturn += "::" + numOfTeaBags + "::" + brewColour;
		return toReturn;
	}
}