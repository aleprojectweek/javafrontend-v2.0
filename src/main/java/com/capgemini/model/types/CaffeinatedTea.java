package com.capgemini.model.types;

public class CaffeinatedTea extends Tea {

	private static final long serialVersionUID = -9037168406058350765L;
	
	protected double caffeineContent;
	
	public CaffeinatedTea(String typeName, int numOfTeaBags, String brewColour, double caffeineContent) {
		super(typeName, numOfTeaBags, brewColour);
		this.caffeineContent = caffeineContent;
	}

	/**
	 * @return the caffeineContent
	 */
	public double getCaffeineContent() {
		return caffeineContent;
	}

	/**
	 * @param caffeineContent the caffeineContent to set
	 */
	public void setCaffeineContent(double caffeineContent) {
		this.caffeineContent = caffeineContent;
	}
	
	@Override
	public String toString() {
		String toReturn = super.toString();
		toReturn += "::" + caffeineContent;
		return toReturn;
	}

}
