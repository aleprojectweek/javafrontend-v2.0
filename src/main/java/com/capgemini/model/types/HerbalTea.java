package com.capgemini.model.types;

public class HerbalTea extends Tea {

	private static final long serialVersionUID = -3641735977264627275L;
	
	protected String medicinalUse;
	
	public HerbalTea(String typeName, int numOfTeaBags, String brewColour, String medicinalUse) {
		super(typeName, numOfTeaBags, brewColour);
		this.medicinalUse = medicinalUse;
	}

	/**
	 * @return the medicinalUse
	 */
	public String getMedicinalUse() {
		return medicinalUse;
	}

	/**
	 * @param medicinalUse the medicinalUse to set
	 */
	public void setMedicinalUse(String medicinalUse) {
		this.medicinalUse = medicinalUse;
	}
	
	@Override
	public String toString() {
		String toReturn = super.toString();
		toReturn += "::" + medicinalUse;
		return toReturn;
	}
}