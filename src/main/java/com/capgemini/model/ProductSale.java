package com.capgemini.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ProductSale")
public class ProductSale {
	//TODO Implement the ORM mapping for Basket
	
	//Declare class fields
	private ProductSaleID id;
	private int quantity;
	private Date productSale;
	
	public ProductSale() {
		//Do nothing
	}
	
	/**
	 * Constructor for the product sale object
	 * @param id
	 * @param quantity
	 * @param productSale
	 */
	public ProductSale(ProductSaleID id, int quantity, Date productSale) {
		this.id = id;
		this.quantity = quantity;
		this.productSale = productSale;
	}
	
	/**
	 * @return the id
	 */
	@EmbeddedId
	public ProductSaleID getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(ProductSaleID id) {
		this.id = id;
	}

	/**
	 * @return the quantity
	 */
	@Column(name="AmountPurchased")
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the productSale
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SaleDate")
	public Date getProductSale() {
		return productSale;
	}

	/**
	 * @param productSale the productSale to set
	 */
	public void setProductSale(Date productSale) {
		this.productSale = productSale;
	}
}