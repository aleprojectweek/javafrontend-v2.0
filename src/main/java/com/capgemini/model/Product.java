package com.capgemini.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.capgemini.model.types.ProductType;

@Entity
@Table(name = "Product")
public class Product {

	//Declare class fields
	private int productID;
	private String productName;
	private String supplierName;
	private int weightInGrams;
	private double price;
	@Transient
	private ProductType type;
	private String serializedType;

	/**
	 * Constructor that creates an empty object
	 */
	public Product() {
		// Do nothing
	}

	/**
	 * Constructor for the Product object with the product id
	 * 
	 * @param productID
	 * @param productName
	 * @param supplierName
	 * @param type
	 */
	public Product(int productID, String productName, String supplierName, double price, int weightInGrams, ProductType type) {
		this(productName, supplierName, price, weightInGrams, type);
		this.productID = productID;
	}
	
	/**
	 * Constructor for the Product object with the product id that takes the serialised type.
	 * @param productID
	 * @param productName
	 * @param supplierName
	 * @param serializedType
	 */
	public Product(int productID, String productName, String supplierName, double price, int weightInGrams, String serializedType) {
		this(productName, supplierName, price, weightInGrams, serializedType);
		this.productID = productID;
		convertType();
	}

	/**
	 * Constructor for the Product object without the product id
	 * 
	 * @param productName
	 * @param supplierName
	 * @param type
	 */
	public Product(String productName, String supplierName, double price, int weightInGrams, ProductType type) {
		this.productName = productName;
		this.supplierName = supplierName;
		this.type = type;
		this.price = price;
		this.weightInGrams = weightInGrams;
	}
	
	/**
	 * Constructor for the Product object without the product id with the serialized type
	 * @param productName
	 * @param supplierName
	 * @param price
	 * @param weightInGrams
	 * @param serializedType
	 */
	public Product(String productName, String supplierName, double price, int weightInGrams, String serializedType) {
		this.productName = productName;
		this.supplierName = supplierName;
		this.serializedType = serializedType;
		this.price = price;
		this.weightInGrams = weightInGrams;
	}

	public void convertType() {
		if(serializedType != null) {
			setType(ProductType.fromString(serializedType));
		}
	}
	
	/**
	 * @return the productID
	 */
	@Id
	@GeneratedValue
	@Column(name = "ProductID")
	public int getProductID() {
		return productID;
	}

	/**
	 * @param productID
	 *            the productID to set
	 */
	public void setProductID(int productID) {
		this.productID = productID;
	}

	/**
	 * @return the productName
	 */
	@Column(name = "ProductName")
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the supplierName
	 */
	@Column(name = "Supplier")
	public String getSupplierName() {
		return supplierName;
	}

	/**
	 * @param supplierName
	 *            the supplierName to set
	 */
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	/**
	 * @return the type
	 */
	@Transient
	public ProductType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(ProductType type) {
		this.type = type;
	}
	
	/**
	 * @return the serializedType();
	 */
	@Column(name = "ProductType")
	public String getSerializedType() {
		return serializedType;
	}
	
	/**
	 * 
	 * @param serializedType
	 */
	public void setSerializedType(String serializedType) {
		this.serializedType = serializedType;
	}

	/**
	 * @return the weightInGrams
	 */
	@Column(name = "Weight")
	public int getWeightInGrams() {
		return weightInGrams;
	}

	/**
	 * @param weightInGrams the weightInGrams to set
	 */
	public void setWeightInGrams(int weightInGrams) {
		this.weightInGrams = weightInGrams;
	}

	/**
	 * @return the price
	 */
	@Column(name = "Price")
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
}