package com.capgemini.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "Dessert")
public class Dessert {

	private int dessertID;
	private String dessertType;
	private boolean hasNuts;
	private boolean hasDairy;
	private double sugarContent;
	private double dessertPrice;
	private int dessertWeight;
	private int recommendedProduct;
	private Date sellByDate;
	
	public Dessert() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the dessertID
	 */
	@Id @GeneratedValue
	@Column(name="DessertID")
	public int getDessertID() {
		return dessertID;
	}

	/**
	 * @param dessertID the dessertID to set
	 */
	public void setDessertID(int dessertID) {
		this.dessertID = dessertID;
	}

	/**
	 * @return the dessertType
	 */
	@Column(name="DesserType")
	public String getDessertType() {
		return dessertType;
	}

	/**
	 * @param dessertType the dessertType to set
	 */
	public void setDessertType(String dessertType) {
		this.dessertType = dessertType;
	}

	/**
	 * @return the hasNuts
	 */
	@Column(name="Nuts")
	@NotNull
	public boolean isHasNuts() {
		return hasNuts;
	}

	/**
	 * @param hasNuts the hasNuts to set
	 */
	public void setHasNuts(boolean hasNuts) {
		this.hasNuts = hasNuts;
	}

	/**
	 * @return the hasDairy
	 */
	@Column(name="Dairy")
	@NotNull
	public boolean isHasDairy() {
		return hasDairy;
	}

	/**
	 * @param hasDairy the hasDairy to set
	 */
	public void setHasDairy(boolean hasDairy) {
		this.hasDairy = hasDairy;
	}

	/**
	 * @return the sugarContent
	 */
	@Column(name="SugerContent")
	public double getSugarContent() {
		return sugarContent;
	}

	/**
	 * @param sugarContent the sugarContent to set
	 */
	public void setSugarContent(double sugarContent) {
		this.sugarContent = sugarContent;
	}

	/**
	 * @return the dessertPrice
	 */
	@Column(name="DessertPrice")
	public double getDessertPrice() {
		return dessertPrice;
	}

	/**
	 * @param dessertPrice the dessertPrice to set
	 */
	public void setDessertPrice(double dessertPrice) {
		this.dessertPrice = dessertPrice;
	}

	/**
	 * @return the dessertWeight
	 */
	@Column(name="DessertWeight")
	public int getDessertWeight() {
		return dessertWeight;
	}

	/**
	 * @param dessertWeight the dessertWeight to set
	 */
	public void setDessertWeight(int dessertWeight) {
		this.dessertWeight = dessertWeight;
	}

	/**
	 * @return the recommendedProduct
	 */
	@Column(name="ProductID")
	public int getRecommendedProduct() {
		return recommendedProduct;
	}

	/**
	 * @param recommendedProduct the recommendedProduct to set
	 */
	public void setRecommendedProduct(int recommendedProduct) {
		this.recommendedProduct = recommendedProduct;
	}

	/**
	 * @return the sellByDate
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SellByDate")
	public Date getSellByDate() {
		return sellByDate;
	}

	/**
	 * @param sellByDate the sellByDate to set
	 */
	public void setSellByDate(Date sellByDate) {
		this.sellByDate = sellByDate;
	}

}
