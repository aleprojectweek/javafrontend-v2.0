package com.capgemini.model;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.model.types.Coffee;
import com.capgemini.model.types.HerbalTea;
import com.capgemini.model.types.ProductType;

public class ProductTypeTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void teaTest() {
		HerbalTea hT = new HerbalTea("Herbal Tea", 45, "Yellow", "Decongestant");
		String serial = hT.toString();
		System.out.println(serial);
		HerbalTea t = (HerbalTea) ProductType.fromString(serial);
		System.out.println(t);
	}
	
	@Test
	public void coffeeTest() {
		Coffee c = new Coffee("Coffee", 2.0, 50.4);
		String serial = c.toString();
		System.out.println(serial);
		Coffee c1 = (Coffee) ProductType.fromString(serial);
		System.out.println(c1);
	}

}
