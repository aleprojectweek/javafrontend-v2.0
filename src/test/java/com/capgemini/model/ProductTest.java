package com.capgemini.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.model.types.HerbalTea;

public class ProductTest {

	private Product product;
	
	@Before
	public void setUp() throws Exception {
		HerbalTea type = new HerbalTea("Caffeinated Tea", 80, "Yellow", "None");
		product = new Product(7, "Green Tea", "Twinings", 2.50, 250, type);
	}

	@Test
	public void constructorTest() {
		assertThat("Check that the product id is set correctly", product.getProductID(), equalTo(7));
		assertThat("Check that the product name is set correctly", product.getProductName(), equalTo("Green Tea"));
		assertThat("Check that the supplier name is set correctly", product.getSupplierName(), equalTo("Twinings"));
		assertThat("Check that the product type exists", product.getType(), notNullValue());
	}
	
	@Test
	public void setProductIDTest() {
		assertThat("Check that the product id is set initally", product.getProductID(), equalTo(7));
		product.setProductID(3);
		assertThat("Check that the product is is set correctly", product.getProductID(), equalTo(3));
	}
	
	@Test
	public void setProductNameTest() {
		assertThat("Check that the product name is set initally", product.getProductName(), equalTo("Green Tea"));
		product.setProductName("English Breakfast Tea");
		assertThat("Check that the product name is set correctly", product.getProductName(), equalTo("English Breakfast Tea"));
	}
	
	@Test
	public void setSupplierNameTest() {
		assertThat("Check that the supplier name is set initally", product.getSupplierName(), equalTo("Twinings"));
		product.setSupplierName("PG Tips");
		assertThat("Check that the supplier name is set correctly", product.getSupplierName(), equalTo("PG Tips"));
	}
}