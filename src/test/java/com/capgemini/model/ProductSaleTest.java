package com.capgemini.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

public class ProductSaleTest {

	private ProductSale productSale;
	
	@Before
	public void setUp() throws Exception {
		ProductSaleID id = new ProductSaleID(3,9);
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, 5);
		cal.set(Calendar.YEAR, 2016);
		
		productSale = new ProductSale(id, 1, cal.getTime());
	}
	
	@Test
	public void constructorTest() {
		assertThat("Check that the productsaleid is set correctly", productSale.getId().getUserID(), equalTo(3));
		assertThat("Check that the productsaleid is set correctly", productSale.getId().getProductID(), equalTo(9));
		assertThat("Check that the quantity is set correctly", productSale.getQuantity(), equalTo(1));
		Calendar cal = Calendar.getInstance();
		cal.setTime(productSale.getProductSale());
		assertThat("Check that the sale date is set correctly", cal.get(Calendar.DAY_OF_MONTH), equalTo(1));
		assertThat("Check that the sale date is set correctly", cal.get(Calendar.MONTH), equalTo(5));
		assertThat("Check that the sale date is set correctly", cal.get(Calendar.YEAR), equalTo(2016));
	}
	
	@Test
	public void setProductSaleIDTest() {
		assertThat("Check that the productsaleid is set initally", productSale.getId().getUserID(), equalTo(3));
		assertThat("Check that the productsaleid is set initally", productSale.getId().getProductID(), equalTo(9));
		ProductSaleID id = new ProductSaleID(1,65);
		productSale.setId(id);
		assertThat("Check that the productsaleid is set correctly", productSale.getId().getUserID(), equalTo(1));
		assertThat("Check that the productsaleid is set correctly", productSale.getId().getProductID(), equalTo(65));
	}
	
	@Test
	public void setQuantityTest() {
		assertThat("Check that the quantity is set initally", productSale.getQuantity(), equalTo(1));
		productSale.setQuantity(4);
		assertThat("Check that the quantity is set correctly", productSale.getQuantity(), equalTo(4));
	}
	
	//TODO Add tests for setting the product sale date
}