package com.capgemini.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class UserTest {

	private User user;

	@Before
	public void setUp() throws Exception {
		user = new User(0, "Alex", "Bance", "alex.bance@capgemini.com", "01234567890", "abance123");
	}

	@Test
	public void constructorTest() {
		assertThat("Test that the uderId is set correctly", user.getUserID(), equalTo(0));
		assertThat("Test that the first name is set correctly", user.getFirstName(), equalTo("Alex"));
		assertThat("Test that the last name is set correctly", user.getLastName(), equalTo("Bance"));
		assertThat("Test that the email is set correctly", user.getEmail(), equalTo("alex.bance@capgemini.com"));
		assertThat("Test that the phone number is set correctly", user.getPhoneNumber(), equalTo("01234567890"));
	}

	@Test
	public void setFirstNameTest() {
		assertThat("Test that the first name is set initally", user.getFirstName(), equalTo("Alex"));
		user.setFirstName("Alan");
		assertThat("Test that the first name is set correctly", user.getFirstName(), equalTo("Alan"));
	}

	@Test
	public void setLastNameTest() {
		assertThat("Test that the last name is set initally", user.getLastName(), equalTo("Bance"));
		user.setLastName("Law");
		assertThat("Test that the last name is set correctly", user.getLastName(), equalTo("Law"));
	}

	@Test
	public void setEmailTest() {
		assertThat("Test that the email is set initally", user.getEmail(), equalTo("alex.bance@capgemini.com"));
		user.setEmail("alan.law@live.co.uk");
		assertThat("Test that the email is set correctly", user.getEmail(), equalTo("alan.law@live.co.uk"));
	}

	@Test
	public void setPhoneNumberTest() {
		assertThat("Test that the phone number is set initally", user.getPhoneNumber(), equalTo("01234567890"));
		user.setPhoneNumber("09876543210");
		assertThat("test that the phone number is set correctly", user.getPhoneNumber(), equalTo("09876543210"));
	}
}