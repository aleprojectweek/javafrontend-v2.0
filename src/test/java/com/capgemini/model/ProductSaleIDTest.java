package com.capgemini.model;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.equalTo;

import org.junit.Before;
import org.junit.Test;

public class ProductSaleIDTest {

	private ProductSaleID id;
	
	@Before
	public void setUp() throws Exception {
		id = new ProductSaleID(3,8);
	}

	@Test
	public void constructorTest() {
		assertThat("Check that the userid is set correctly", id.getUserID(), equalTo(3));
		assertThat("Check that the productID is set correctly", id.getProductID(), equalTo(8));
	}
	
	@Test
	public void setUserIDTest() {
		assertThat("Check that the userid is set initally", id.getUserID(), equalTo(3));
		id.setUserID(5);
		assertThat("Check that the userid is set correctly", id.getUserID(), equalTo(5));
	}
	
	@Test
	public void setProductIDTest() {
		assertThat("Check that the productid is set initally", id.getProductID(), equalTo(8));
		id.setProductID(34);
		assertThat("Check that the productid is set correctly", id.getProductID(), equalTo(34));
	}

}