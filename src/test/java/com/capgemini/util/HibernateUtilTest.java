package com.capgemini.util;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class HibernateUtilTest {

	@Test
	public void buildSessionFactoryTest() {
		assertThat("Assert that buildSessionFactory is called and sessionFactory is populated!", HibernateUtil.getSessionFactory(), notNullValue());
	}

}
