package com.capgemini.util;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.notNullValue;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.model.Product;

public class DBQueryManagerTest {

	@Before
	public void setUp() throws Exception {
	}

	
	@Test
	public void getAllProductTest() {
		List<Product> products = DBQueryManager.getAllProducts();
		for(Product p:products) {
			System.out.println(p.getProductName());
		}
		assertThat("Check that the list is not null", products, notNullValue());
	}
	
	@Test
	public void serializationTest() {
		
	}

}
