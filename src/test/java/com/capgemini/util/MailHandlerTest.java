package com.capgemini.util;

import static org.junit.Assert.*;

import javax.mail.MessagingException;

import org.junit.Before;
import org.junit.Test;

public class MailHandlerTest {

	private MailHandler mail;
	
	@Before
	public void setUp() throws Exception {
		mail = new MailHandler();
	}
	
	@Test
	public void sendEmailTest() {
		try {
			mail.sendEmail("Test", "This is a test message!", "alex.bance@live.co.uk");
		} catch (MessagingException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
	}

}
